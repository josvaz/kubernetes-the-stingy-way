#!/bin/bash
set -euo pipefail

source ./shlib/run.sh

echo "Tear down VMs"
./placeholder-files.sh
(cd vagrant && vagrant destroy -f)

echo "Removing certificate files"
rm -f certs/*.pem certs/*.csr certs/worker*-csr.json

echo "Removing configuration files"
rm -f configs/*.kubeconfig

echo "Removing encryption configuration"
rm -f cypher/encryption-config.yaml

echo "Removing provisioned IP records"
rm -f vagrant/vip vagrant/*_ip

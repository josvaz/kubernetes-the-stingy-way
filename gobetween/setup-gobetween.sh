#!/bin/bash
set -e

echo 'Install dependencies'
sudo apt-get update
sudo apt-get -y install gettext-base

echo 'Download gobetween binary'
tgz_url=$(curl -s https://api.github.com/repos/yyyar/gobetween/releases \
  | grep browser_download_url | grep linux_amd64 | cut -d '"' -f 4 \
  | sort -r | head -1)
wget -q --show-progress --timestamping "${tgz_url}"

tar -zxvf gobetween*.tar.gz

echo 'Install gobetween binary'
install gobetween /usr/local

echo "Setup gobetween configuration"
mkdir -p /etc/gobetween/
GOBETWEEN_STATIC_LIST="    \"$(cat /vagrant/controller-0_ip):6443\""
MAX_CONTROLLER=$((${NO_CONTROLLERS:=3}-1))
for index in $(seq 0 $MAX_CONTROLLER);do
  ip=$(cat /vagrant/controller-${index}_ip)
  GOBETWEEN_STATIC_LIST+=",
    \"${ip}:6443\""
done
export GOBETWEEN_STATIC_LIST
envsubst < /tmp/gobetween/gobetween.toml.tmpl > /etc/gobetween/gobetween.toml

echo "Setup gobetween systemd unit"
cp /tmp/gobetween/gobetween.service /etc/systemd/system

echo "Start gobetween"
sudo systemctl daemon-reload
sudo systemctl enable gobetween
sudo systemctl start gobetween

systemctl status gobetween

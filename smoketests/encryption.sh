#!/bin/bash
set -euo pipefail

source ../shlib/run.sh

kubectl create secret generic kubernetes-the-stingy-way \
  --from-literal="mykey=mydata"

run_in_node controller-0 sudo ETCDCTL_API=3 etcdctl get \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem\
  /registry/secrets/default/kubernetes-the-stingy-way | hexdump -C

kubectl delete secret kubernetes-the-stingy-way

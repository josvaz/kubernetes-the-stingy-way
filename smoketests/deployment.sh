#!/bin/bash
set -euo pipefail

source ../shlib/kubewait.sh

kubectl create deployment nginx --image=nginx
wait_running 1 kubectl get pods -l app=nginx

POD_NAME=$(kubectl get pods -l app=nginx -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward "$POD_NAME" 8080:80 &
FORWARDER_PID=$!

sleep 1 # If we don wait the port forwarding migth not be ready
curl --head http://127.0.0.1:8080

echo 'Deployment logs'
kubectl logs "$POD_NAME"

echo 'Service'
kubectl expose deployment nginx --port 80 --type NodePort
NODE_PORT=$(kubectl get svc nginx \
            --output=jsonpath='{range .spec.ports[0]}{.nodePort}')
EXTERNAL_IP=$(cat ../vagrant/worker-0_ip)
curl -I http://${EXTERNAL_IP}:${NODE_PORT}

kubectl delete deployment nginx
kubectl delete service nginx
kill "$FORWARDER_PID"

#!/bin/bash
set -euo pipefail

source ../shlib/kubewait.sh
source ../shlib/run.sh

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: untrusted
  annotations:
    io.kubernetes.cri.untrusted-workload: "true"
spec:
  containers:
    - name: webserver
      image: gcr.io/hightowerlabs/helloworld:2.0.0
EOF

wait_running 1 kubectl get pods -o wide untrusted

INSTANCE_NAME=$(kubectl get pod untrusted --output=jsonpath='{.spec.nodeName}')

run_in_node "${INSTANCE_NAME}" sudo runsc --root  /run/containerd/runsc/k8s.io list

crictl_output=$(run_in_node "${INSTANCE_NAME}" sudo crictl -r \
	unix:///var/run/containerd/containerd.sock pods --name untrusted -q)
POD_ID=$(echo "${crictl_output}" | sed "s/$\'//g" |sed "s/\r\'//g")
echo "The untrusted POD_ID is ${POD_ID}"

crictl_output=$(run_in_node "${INSTANCE_NAME}" sudo crictl -r \
	unix:///var/run/containerd/containerd.sock ps -q -p "${POD_ID}")
CONTAINER_ID=$(echo "${crictl_output}" | sed "s/$\'//g" |sed "s/\r\'//g")
echo "The untrusted CONTAINER_ID is ${CONTAINER_ID}"

run_in_node "${INSTANCE_NAME}" sudo runsc \
  --root /run/containerd/runsc/k8s.io ps "${CONTAINER_ID}"

kubectl delete pod untrusted


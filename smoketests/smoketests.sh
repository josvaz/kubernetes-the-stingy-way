#!/bin/bash
set -euo pipefail

./encryption.sh
./deployment.sh
./untrusted.sh

echo "Tests passed"


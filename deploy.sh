#!/bin/bash
set -euo pipefail

source ./cluster-config
source ./shlib/run.sh

echo "Starting VMs"
./placeholder-files.sh
(cd vagrant && vagrant up --no-provision)

echo "Generating certificates"
(cd certs && ./regen-certs.sh)

echo "Preparing configuration files"
(cd configs && ./configure.sh)

echo "Preparing encryption configuration"
(cd cypher && ./create-encryption-config.sh)

echo "Provisioning"
(cd vagrant && vagrant provision)

echo "Verify setup"
curl --cacert ./certs/ca.pem "https://$(cat vagrant/vip):6443/version"
(cd certs && ./setup-kubectl.sh)

echo "DNS cluster add-on"
(cd coredns && ./setup-coredns.sh)

echo "Smoketests"
(cd smoketests && ./smoketests.sh)

# Smoke Test

The smoke tests are the last step of the deployment, in the [deploy.sh](/deploy.sh) script:

```bash
echo "Smoketests"
(cd smoketests && ./smoketests.sh)
```

The [smoketests.sh](/smoketests/smoketests.sh) script is basically a script version of the 3 original tests explained in the [original smoke tests lab](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/13-smoke-test.md):

```bash
./encryption.sh
./deployment.sh
./untrusted.sh
```

Next: [Cleaning up](13-cleanup.md)

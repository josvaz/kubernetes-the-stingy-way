# Configuring kubectl for Remote Access

The kubectl client setup is done outside Vagrant, right after the vagrant provisioning, as you can see at the [/deploy.sh](/deploy.sh) script:

```bash
...
echo "Provisioning"
(cd vagrant && vagrant provision)

echo "Verify setup"
curl --cacert ./certs/ca.pem "https://$(cat vagrant/vip):6443/version"
(cd && certs ./setup-kubectl.sh)
...
```

So the [/certs/setup-kubectl.sh](/certs/setup-kubectl.sh) script takes care of the client side kubectl config, and ends by using it to check the lists of cluster components and worker nodes.

Next: [Provisioning Pod Network Routes](/docs/10-pod-network-routes.md)

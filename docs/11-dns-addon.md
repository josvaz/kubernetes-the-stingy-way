# Deploying the DNS Cluster Add-on

The coredns setup is not part of the Vagrant code. It is done from the cluster client side and executed in the `deploy.sh` after the Vagrant provisioning step, the cluster verification and `kubectl` setup:

```bash
echo "Provisioning"
(cd vagrant && vagrant provision)

echo "Verify setup"
curl --cacert ./certs/ca.pem "https://$(cat vagrant/vip):6443/version"
(cd certs && ./setup-kubectl.sh)

echo "DNS cluster add-on"
(cd coredns && ./setup-coredns.sh)
```

The [setup-coredns.sh](/coredns/setup-coredns.sh) uses a local copy of the code DNS yaml at [coredns/coredns.yml](/coredns/coredns.yaml) from https://storage.googleapis.com/kubernetes-the-hard-way/coredns.yaml
In the [original lab for this part](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/12-dns-addon.md) the YAML URL is used directly instead, which could potentially break the scripts here.

Then the script applies the yaml over the cluster with `kubectl apply -f coredns.yaml` and then it basically verifies it is working as expected.

Next: [Smoke Test](/docs/12-smoke-test.md)

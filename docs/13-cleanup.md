# Cleaning up

Clean up is somewhat simpler in this case than [the original lab](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/14-cleanup.md). The [cleanup.sh](/cleanup.sh) script just tears down the vagrant deployment:

```bash
(cd vagrant && vagrant destroy -f)
```

And then just cleans up the files left in the client side:

```bash
echo "Removing certificate files"
rm -f certs/*.pem certs/*.csr certs/worker*-csr.json

echo "Removing configuration files"
rm -f configs/*.kubeconfig

echo "Removing encryption configuration"
rm -f cypher/encryption-config.yaml

echo "Removing provisioned IP records"
rm -f vagrant/vip vagrant/*_ip
```

And that's it.

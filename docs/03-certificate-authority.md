# Provisioning a CA and TLS Certificates

As in the original lab you will provision a PKI Infrastructure using CloudFlare's PKI toolkit, cfssl.

All actions are automated, as with the rest of the deployment, and the entry point is the script:

[/certs/regen-certs.sh](/certs/regen-certs.sh)

That script creates each certificate required in the same order as in the original lab. There is an individual script called for each step:

- [CA](/certs/regen-ca.sh): The Certificate Authority used to generate all the rest certificates. Loads [ca-csr](/certs/ca-csr.json).
- [Admin certificate](/certs/regen-admin.sh): The server side certificate. Loads [admin-csr](/certs/admin-csr.json).
- [Client certificates](/certs/regen-client-certs.sh): The client certificates for each worker. They generate the `csr` file on the fly.
  - Note that in this setup the external and internal IPs are the same.
  - The internal IPs to the compute instances are reachable both from the host and between the compute nodes.
- [Controller Manager certificate](/certs/regen-controller-manager-cert.sh): For the controller manager client, running the main Kubernetes control loop. Uses [kube-controller-manager-csr.json](/certs/kube-controller-manager-csr.json)
- [kube-proxy certificate](/certs/regen-kube-proxy-cert.sh): For the network proxy running on each node. Loads [kube-proxy-csr](/certs/kube-proxy-csr.json)
- [kube-scheduler certificate](/certs/regen-kube-scheduler-cert.sh): For the pod scheduler. Loads [kube-scheduler-csr](/certs/kube-scheduler-csr.json)
- [kubernetes certificate](/certs/regen-kubernetes-cert.sh): For the cluster API interface. Loads [kubernetes-csr](/certs/kubernetes-csr.json).
  - This certificate SAN (Subject Alternate Name includes aliases for all places the API can be invoked from:
    - The public interface will use the service VIP we configured for it, for when called from outside the cluster.
    - Localhost IP for when calling the API within the same node.
    - The API cluster API for calls from a running Pod, which have a different netwpork view. {Confirm}
    - The constant `kubernetes.default`.
    - And each of the public and private controller IP addresses. Used when the API call goes to those.
- [Service Account](/certs/regen-service-account.sh): For the kubernetes service account. Loads [service-account-csr](/certs/service-account-csr.json)

All scripts, but the CA itself, load [ca-config](/certs/ca-config.json).

Those are a lot of certificates, because the full kubernetes installation is a bit more complicated your typical client-server setup.

Let's recap how all the different kubernetes componets fit together at high level:

![docs/k8s-diagram.png](docs/k8s-diagram.png "Kubernetes architecture diagram")

A kubernetes cluster is usually group of nodes, each node can be a:
- **Master** making up the kubernetes **control plane**. There can be one or more master nodes.
- **Worker node** where containerized user workloads can run.

Inside each **master** node there are several components with different roles and responsabilities:
- `etcd` is usually the distrubuted database used to store all persistent data managed by the **control plane**. This component allows the rest to be stateless and behave the same regardless on which of the controller nodes they are running, as etcd exposes a unified state across all nodes. The numer of etcd nodes must be odd so to avoid split brain conditions in case of a network cut. Because only one group may reach quorum, that is, represent more than half the working nodes.
  - `etcd` processes connect to other `etcd` processes running on other master nodes.
- `kube-apiserver` exposes the Kubernetes API, it is the frontend of the **control plane**. By using `etcd` underneath the API can scale horizontaly, just by more controller nodes running more api servers.
  - The API servers connect to the local `etcd` to store and read back persistent state.
- `kube-controller-manager` runs kubernetes controllers, which are in charge of making sure the real state of user workloads meet their requested state. This component manages nodes, replication, endpoints and service accounts & tokens. All of this work is done through the API.
  - The controller connects to the local `API server`.
- `kube-scheduler` watches newly created pods that have no node assigned, and selects a node for them to run on. Does all this also throught the API.
  - The scheduler also connects to the local `API server`.

Inside each **worker node** there are also several components:
- A `container runtime` like Docker or similar.
- `kubelet` agent that makes sure that containers are running in a pod. It connects an `API server` and to the local `container runtime`.
- `kube-proxy` manages the kubernetes services and networking side of things for the pods in the node. Connexts to the local `container runtime`.

Going back to the certificates we created:
- We need the `CA` as our center of authority and trust, that signs the rest of certificates.
- The `kubernetes certificate` is the API server side certificate that needs to be accessed from the rest of components. This requires setting up a lot of Subject Aletrnative anmes for:
  - All connections from inside and outside the master nodes.
  - All endpoints master nodes.
- Most of the rest of the certificates are client side ones to be able to connect to the API servers:
  - The `admin` cert and private key are used from the `kubectl` client to be able to administer the k8s cluster.
  - The `client certificates per worker node` are used to setup the local `kubectl` environment and `kubelet` process.
  - The `controller manager certificates` are used to setup the local `kubectl`'s controller manager credentials on master nodes.
  - The `kube proxy certificates` are used to setup the local `kubectl`'s proxy credentials on master nodes.
  - The `service account certificate` is configured in the API server to allow access to the API, while the key is configured in the Controller Manager.

## Certificate distribution

The certificate distribution is done later from the Vagrant file.

For the **controller nodes** the Vagrant Ruby code is something like this:

```ruby
  %w[ca.pem
     ca-key.pem
     kubernetes.pem
     kubernetes-key.pem
     service-account.pem
     service-account-key.pem].each do |filename|
    node.vm.provision :file,
                      source: "../certs/#{filename}",
                      destination: "~/#{filename}"
  end
```

For the **worker nodes**:

```ruby
  ['ca.pem', "worker-#{i}.pem", "worker-#{i}-key.pem"].each do |filename|
    node.vm.provision :file,
                      source: "../certs/#{filename}",
                      destination: "~/#{filename}"
  end
```

The files are copied at **Vagrant provisioning time**, which happens **after**:

1. Initial vagrant node activation, without provisioning.
2. Generating the certificates, with `./regen-certs.sh`.

```bash
echo "Starting VMs"
./placeholder-files.sh
(cd vagrant && vagrant up --no-provision)

echo "Generating certificates"
(cd certs && ./regen-certs.sh)

echo "Preparing configuration files"
...

echo "Preparing encryption configuration"
...

echo "Provisioning"
(cd vagrant && vagrant provision)
```

Next: [Configuration files for authentication](/docs/04-configuration-files.md)

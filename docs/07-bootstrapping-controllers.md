# Bootstrapping the Kubernetes Control Plane

This section describes how the Control Plane components API Server, Scheduler, and Controller Manager, similarly to [the original section](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/08-bootstrapping-kubernetes-controllers.md). But here again we use Vagrant and some bash scripts complete the tasks. Also the load balancer setup is quite different, as we don't have cloud Load Balancers we use a extra node and install a go based TCP load balancer instead: [gobetween](http://gobetween.io/).

Again there is a script dealing with all the control plane binaries installation & setup: [/control-plane/control-binaries.sh](/control-plane/control-binaries.sh)

This is invoked from the vagrant file also at **Vagrant provisioning time** right after the etcd setup:
```ruby
      node.vm.provision :shell,
                        path: '../control-plane/control-binaries.sh',
                        args: [i.to_s]
```
Note the script takes as an argument the index of the etcd/control plane node.

But that is not all, there is also the healtz and RBAC setup:
```ruby
      node.vm.provision :shell, path: '../control-plane/healtz.sh'
      next if i < 2

      node.vm.provision(:shell, path: '../control-plane/rbac-setup.sh')
```
Note that the RBAC setup is only done for the last controller node index.

The [/control-plane/healtz.sh](/control-plane/healtz.sh) script installs nginx & to reverse proxy to the health check endpoint and test it.

The [/control-plane/rbac-setup.sh](/control-plane/rbac-setup.sh) is a Kubernetes configuration to setup proper admin access to the kubernetes user. This only needs to be done once, and only after all control plane nodes are setup. That is the reason it is only executed in the last node.

The load balancer setup takes a full node definition in Vagrant:
```
  [0].each do |i|
    define_node(config, 'lb', i, LBS_OFFSET) do |node|
      memory_setup(node, LB_MEMORY_MB)
      node.trigger.after :up do |trigger|
        trigger.name = "Record VIP: #{VIP}"
        trigger.run_remote = { inline: "echo #{VIP} > /vagrant/vip" }
      end
      node.vm.network :private_network, bridge: BRIDGED_IFACE, ip: VIP
      Dir.glob('../gobetween/gobetween.*') do |src|
        dst = File.join('/tmp/gobetween/', File.basename(src))
        node.vm.provision(:file, source: src, destination: dst)
      end
      node.vm.provision(:shell, path: '../gobetween/setup-gobetween.sh')
    end
  end
```

That node installs `gobetween` with [/gobetween/setup-gobetween.sh](/gobetween/setup-gobetween.sh).

This setup does not include High Availability, but that was also not explained in the original guide, as the cloud LBs take care of that themselves. This is just a local setup, so the High Availability would be more fake than real, similarly to how the 3 node control plane is more for educational purposes than anything else.

If this setup was to be moved to something more productio-ready, like bare metal nodes instead of VMs, or at least nodes run on different hardware that can fail independently of each other, then it may make sense to upgrade to a HA setup. You deploy at least 2 LB nodes and install on them gobetween instances sharing the same Virtual IP or move to something else like [seesaw](https://medium.com/@eric.ahn/seesaw-101-scalable-and-robust-load-balancing-1-36c1846b3b0c). There are plenty of open source options for this.

Next: [Bootstrapping the Kubernetes Worker Nodes](08-bootstrapping-workers.md)
# Configuration files for authentication

The entry point for all these configurations is:

[/configs/configure.sh](/configs/configure.sh)

That script includes configuration for:

- The workers
- The kube-proxy
- The kube-controller-manager
- The scheduler
- And kube-admin

Basically these map directly to each of the sections described in the original laboratory section [05-kubernetes-configuration-files](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/05-kubernetes-configuration-files.md)

These files are copied in the nodes right after the certificates, also at **Vagrant prvisioning time**.

For the controller nodes:

```ruby
    %w[admin.kubeconfig
        kube-controller-manager.kubeconfig
        kube-scheduler.kubeconfig].each do |filename|
    node.vm.provision :file,
                        source: "../configs/#{filename}",
                        destination: "~/#{filename}"
    end
```

For the workers:

```ruby
    ["worker-#{i}.kubeconfig", 'kube-proxy.kubeconfig'].each do |filename|
    node.vm.provision :file,
                        source: "../configs/#{filename}",
                        destination: "~/#{filename}"
    end
```

Next: [Data Encryption Config and Key](/docs/05-encryption-keys.md)

# Provisioning Pod Network Routes

The worker routes are setup quite differently in this case compared to the [original lab](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/11-pod-network-routes.md).

The Vagrantfile prepares the routes data and passes it to a `setup-routes.sh` script:
```ruby
      node.vm.provision(:shell,
                        path: '../workers/setup-routes.sh',
                        args: [
                          node_address(PRIVATE_NETWORK, address_offset, i),
                          worker_routes_for(i, address_offset)
                        ])
```
That is the last step in the vagrant worker node setup code.

The `setup-routes.sh` script takes 2 arguments:

- **The current worker IP address**
  That is calculated from the prefix `192.168.50` and then the sum of the address offset and the node index. For workers the address offset is `WORKERS_OFFSET = 30` so workers IP are `192.168.50.30, .31, etc` (as indexes start at 0). Here is the `node_address` method:
  ```ruby
    def node_address(network, address_offset, index)
      "#{network}.#{address_offset + index}"
    end
  ```

- **The routers to setup from that worker**
  Each worker needs a route per existing worker in the cluster. Each route here is in the format `{Pod CIDR}:{worker IP}` and all routes are returned separated by commas:
  ```ruby
    def worker_routes_for(index, address_offset)
      routes = []
      sequence_for(WORKERS).each do |i|
        next if i == index

        routes << "#{pod_cidr(POD_CIDR_PREFIX, i + 1)}:"\
                  "#{node_address(PRIVATE_NETWORK, address_offset, i)}"
      end
      routes.join(',')
    end
  ```

  **The Pod CIDR on each route** is calculated from as `10.200.{index}.0/24`:
  ```ruby
    def pod_cidr(pod_cidr_prefix, index)
      "#{pod_cidr_prefix}.#{index}.0/24"
    end
  ``` 

Then [setup-routes.sh](/workers/setup-routes.sh) takes this inputs and uses them to setup `netplan` on the ubuntu bionic VM accordingly:

- First it needs to translate this worker IP (first argument passed in) into its interface device name with the aid of the `ip` tool:
  ```bash
  interface_name=$(ip address | grep "inet ${IP_ADDRESS}" -B2 \
    |grep 'mtu ' | awk '{print $2}' |tr -d ':')
  ```

- Then it prepares a `netplan` routes spec YAML file for that device:
  ```bash
  cat <<EOF | sudo tee /etc/netplan/99-routes.yaml
  network:
    ethernets:
      ${interface_name}:
        routes:
  EOF
  ```

- Finally adds the routes to the YAML parsed from the second argument, a comma separated list of routes`{Pod CIDR}:{worker IP}`:
  ```bash
  for pair in $(echo "${ROUTES}" | tr ',' '\n'); do
    IFS=":" read -r -a route <<< "$pair"
    cat <<EOF | sudo tee -a /etc/netplan/99-routes.yaml
          - to: ${route[0]}
            via: ${route[1]}
  EOF
  done
  ``` 

At the end of the script `netplan apply` needs to be called so that the routes take effect.

Next: [Deploying the DNS Cluster Add-on](/docs/11-dns-addon.md)

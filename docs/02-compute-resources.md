# Compute Resources

This is probably the most different section compared against the [original's compute resources lab](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/03-compute-resources.md).

No `gcloud` here, all our compute resources run on a Virtual Box setup software defined thanks to [vagrant](https://www.vagrantup.com/intro/index.html).

**Why use Vagrant for this?**

- I wanted an scripted solution, something I could bring up and down quickly with ease. We can call it "software defined" as that sounds cooler these days.
- I expected scripting virtual box directly would be quite involved. It will probably introduce too much knowledge that is not really related to the kubernetes cluster installation problem at hand.
- Vagrant not only abstracts managing virtual box, it also would allow to move to other providers, like `VMware`or `Hyper-V`.

**Why not Docker instead?**

While Docker has probably displaced Vagrant nowadays for most developer's use cases, for this particular one, it is probably not better suited.

As we are building a Kubernetes cluster the worker nodes we setup here will have to run container inside or them. That is already 1 level of inception and complexity you might want to avoid. Then imgine you actually use this cluster for some development and you end up needed again Docker in Docker in the workload!... nah! better avoid such complications.

It would be interesting, though, to try this setup on a Docker Vagrant provider and see how it fares...

**Why not a configuration tool, like Ansible?**

The scope of this project is basically learning purposes, maybe a bit of development. No more.

In such a scope there is value for a deployment that can "fake" a whole cluster over a single hardware box. It simplifies the setup and helps keeping the focus on the actual issue at hand, kubernetes cluster installation and configuration.

Configuration tools like Ansible are best suited to deploy over a network of nodes. It will be an option for a proper on-prem production setup for Kubernetes. But then again, in such scope you might find other more specialiced kubernetes installation and deployment options more appealing.

## Vagrant file

Most of the compute provisioning decisions and setup can be found on the [Vagrantfile](/vagrant/Vagrantfile).

That is Ruby source code, but no need to read it all at once now. This documentation will walk through the interesting bits as needed.

## Networking

While the original lab sets up a VPC for all nodes in the Kubernetes cluster, in our Vagrant model we put them all instead on the same [private network](https://www.vagrantup.com/docs/networking/private_network.html).

Such `private network` is provided by Virtual Box as a [Host only network](https://www.virtualbox.org/manual/ch06.html#network_hostonly). This is convenient for our setup for several reasons:
- All cluster nodes can access each other. Just as in the [flat network model Kubernetes assumes](https://kubernetes.io/docs/concepts/cluster-administration/networking/#kubernetes-model) for the controllers and worker nodes.
- You host machine can access then easily via `vagrant ssh {nodename}`, which is as convenient as the cloud setup.
- The network is private to your host, so you don have to worry about external access or extra security lockdown.
- It avoid most conflicts with your external networking.
  - If you already have a network `192.168.50.0/24` in your routes or VPNs, you can just change that constant at the begining the [Vagrantfile](/vagrant/Vagrantfile) to make it work.

The `private network` while convenient, has a serious limitation, the nodes can not reach the internet. That would make software installation difficult. In order to deal woth that, all nodes get another [Virtual Box NAT interface](https://www.virtualbox.org/manual/ch06.html#network_nat) that basically provides them internet access, transparently through your host.

### Firewall Rules

With the described network setup above there is little need for firewalling rules, as the cluster network is private already and the nat interfaces won't allow ingress access, specially through your host.

One could argue that it would be best to allow only access to the SSH port and maybe 6443. And one would be right, in this setup those will translate to per node `iptable` firewall rules (or `ufw` as these are ubuntu nodes). But we chose to skip such configuration.

We could say we left it as an exercise for the reader.

## Kubernetes Public IP Address

The public cluster IP or VIP as we refer to it in our code is yet another private network address: `192.168.50.100`
You have to change it in case you already use a `192.168.50.0/24` network somewhere else. This is again for conveninience and use only from your host.

In case you would want this to be accessible from your network you could try that IP to be on a [Virtual Box bridged network interface](https://www.virtualbox.org/manual/ch06.html#network_bridged). That setup is not straightforward though, as Vagrant only supports DHCP on [public networks](https://www.vagrantup.com/docs/networking/public_network.html), the ones backed by bridged interfaces. So you would want to configure your DHCP server to give that interface a static IP... so much for software defined infrastructure.

Another option would be to move the load balancer up to your hardware box, so you would access the setup from there.

Again having the cluster completely isolated inside your own box makes the configuration easier and more reproducible, and greately reduces the need for firewalling rules.

## Compute Instances

These in our case are running on [ubuntu/bionic64](https://app.vagrantup.com/ubuntu/boxes/bionic64) Vagrant boxes. So we run also on `Ubuntu version 18.04`, just like the original lab.

The Vagrantfile assigns fixes IPs to the nodes. They are organized per type in jumps of 10 for each group:

- Load Balancer is at `192.168.50.10`, and there is only one.
- Controllers start from `192.168.50.20`, then `.21` and `.22`.
- Workers start from `192.168.50.30`...

### Bringing up the instances

To bring up the compute instances without actually installing anything on them, the `deploy.sh` code is doing this:

~~~bash
#!/bin/bash
set -euo pipefail

source ./cluster-config
source ./shlib/run.sh

echo "Starting VMs"
./placeholder-files.sh
(cd vagrant && vagrant up --no-provision)
...
~~~

1. First the code [prepares the bash script to fail on any error](https://explainshell.com/explain?cmd=set+-euxo%20pipefail).
2. Then it loads the [cluster configuration as a small set of environment variables](/cluster-config).
3. It also loads some [common bash functions](/shlib/run.sh), which basically allow it to run commands on a folder or inside a vagrant node.
4. Before bringing up the compute nodes, [it needs to prepare some empty files, or placeholders](/placeholder-files.sh). Those files are to be read only at provision time, when installing & configuring the software, but Vagrant will check they exist even while skipping provisioning. So to trick Vagrant into thinking they exist, they are created as empty files, and filled later with data at node launch time, so they can be used at provision time.
5. Finally **we run vagrant's instance deployment skipping privisioning of the nodes**: `vagrant vagrant up --no-provision` Note that command needs to be run within the `vagrant/` folder.


#### Verification

```bash
~/kubernetes-the-stingy-way$ cd vagrant/
~/kubernetes-the-stingy-way/vagrant$ vagrant status
Current machine states:

controller-0              running (virtualbox)
controller-1              running (virtualbox)
controller-2              running (virtualbox)
worker-0                  running (virtualbox)
worker-1                  running (virtualbox)
lb-0                      running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

#### SSH Access

~~~bash
~/kubernetes-the-stingy-way/vagrant$ vagrant ssh controller-1
Welcome to Ubuntu 18.04.2 LTS (GNU/Linux 4.15.0-55-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Jul 27 21:31:17 UTC 2019

  System load:  0.12              Processes:             102
  Usage of /:   16.4% of 9.63GB   Users logged in:       0
  Memory usage: 38%               IP address for enp0s3: 10.0.2.15
  Swap usage:   0%                IP address for enp0s8: 192.168.50.21


0 packages can be updated.
0 updates are security updates.

vagrant@controller-1:~$
~~~

Very easy, you don't even need to remember the node IPs!

Next: [Provisioning a CA and TLS Certificates](/docs/03-certificate-authority.md)

# Bootstrapping the etcd Cluster

As already stated the etcd cluster processes **provide state** and **high availability** to the Kubernetes control plane. Thus it is the first component to deploy, as all others depend on it.

The script [/etcd/setup-etcd.sh](/etcd/setup-etcd.sh) is to be run on each of the controller nodes. It takes the controller node index as an input parameter and basically reproduces the instructions from the original lab section [07-bootstrapping-etcd.md](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/07-bootstrapping-etcd.md)

The actual execution of the script on each node is again delayed until **Vagrant provisioning time**:

```ruby
node.vm.provision :shell, path: '../etcd/setup-etcd.sh', args: [i.to_s]
```
Note the script takes as an argument the index of the etcd target node.

This goes between the encryption configuration and control binaries installation.

Next: [Bootstrapping the Kubernetes Control Plane](07-bootstrapping-controllers.md)

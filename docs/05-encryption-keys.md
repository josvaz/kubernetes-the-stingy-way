# Data Encryption Config and Key

The keys configured here allow to encrypt Kubernetes secrets, as well as cluster state or application configurations.

This section code is pretty simple: [/cypher/create-encryption-config.sh](/cypher/create-encryption-config.sh).

```bash
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)

cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
```

Just create the key and the associated configuration, exactly as stated at the original lab at [06-data-encryption-keys.md](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/06-data-encryption-keys.md)

The distribution is done later at **Vagrant provisioning time** but this time only for the controller nodes:

```ruby
    node.vm.provision :file,
                    source: '../cypher/encryption-config.yaml',
                    destination: '~/encryption-config.yaml'
```

Next: [Bootstrapping the etcd Cluster](/docs/06-bootstrapping-etcd.md)

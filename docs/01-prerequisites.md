# Prerequisites

This maps to 2 original's labs:

- [Prerequisites](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/01-prerequisites.md)
- [Installing the Client Tools](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/02-client-tools.md)

As there is no cloud client setup in this case, they get squashed both into one section.

This setup has been tested to work on Linux with [preinstaled VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [vagrant](https://www.vagrantup.com/downloads.html).

The rest of the document will assume you are running on a similar environment with both tools already installed. If you are running on Mac, expect some small differences on the client side of things. The rest will run on vagrant linux boxes anyway.

Apart from that, before running the scripts, we need to install the client certificate tools, just like in the original lab. As cfssl is written in Go, distributed as statically linked binaries, the steps are really straing forward:

Original contents of `install-cfssl.sh`:

~~~bash
#!/bin/bash
wget -q --show-progress --https-only --timestamping \
  https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 \
  https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
chmod +x cfssl_linux-amd64 cfssljson_linux-amd64
sudo mv cfssl_linux-amd64 /usr/local/bin/cfssl
sudo mv cfssljson_linux-amd64 /usr/local/bin/cfssljson
~~~

Basically get the binaries for the target plaform, linux in this case, set execution permissions and move them (using sudo) to their final paths, that should appearn in your `$PATH`.

Next: [Compute Resources](docs/02-compute-resources.md)
# Bootstrapping the Kubernetes Worker Nodes

Worker nodes are setup after the controller ones in Vagrant. Also the actual worker binaries are setup between the worker configuration and the pod network route setup:
```ruby
      node.vm.provision(:shell,
                        path: '../workers/setup-workers.sh',
                        args: [
                          KUBERNETES_VERSION,
                          CRI_VERSION,
                          pod_cidr(POD_CIDR_PREFIX, i + 1)
                        ])
```

The [/workers/setup-workers.sh](/workers/setup-workers.sh) script does the actual binaries download & installation, in a very similar way to the [original lab](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/09-bootstrapping-kubernetes-workers.md).

Next: [Configuring kubectl for Remote Access](09-setup-kubectl.md)

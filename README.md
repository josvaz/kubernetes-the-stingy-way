# Kubernetes the Stingy Way

This is what would happen to [Kelsey's Kubernetes the Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way) if you wanted to avoid all cloud expenses and try to run all locally.

## Target Audience

The target audience for this tutorial is someone planning to run a Kubernetes cluster and wants to understand how everything fits together. In addition to that, that person also wants to avoid cloud expenses or have full local control of the environment to experiment on it.

Another reason might be that the target Kubernetes cluster is fully on-premise and this could be a closer developer version of the final setup. 

## Deployment Details

Still a decent development box is required, with at least 6 spare GiB around, to deploy all the VM nodes. The minimal default setup is:
* **3 controllers** to simulate high availability in the administration side, etcd needs 3 nodes minumum to ensure quorum.
* **2 worker nodes**, one less than in the original cloud based setup. This still simulates "several" worker nodes, like a real environment, but reduces the memory footprint.
* **1 lb node**, fronting the kubernetes cluster API. This VM is setup with 512MB only by default.

This infrastructure as code is deployed with [vagrant](https://www.vagrantup.com/) using [VirtualBox](https://www.virtualbox.org/) as provider.

Apart from vagrant and Virtualbox, Kubernetes The Stingy Way installs the following components:

* Kubernetes
* containerd Container Runtime
* gVisor 
* CNI Container Networking
* etcd
* CoreDNS

## Code Map

The main code entry points for this repository are:

1. `install-cfssl.sh` to install the SSL tools used to setup the cluster certificates.
1. `deploy.sh` to bring the whole kubernetes cluster up.
1. `cleanup.sh` to bring it down and clean up resources.

## Documentation Organization

The documentation of this repo will follow the preparation, deployment and cleanup code. It will focus on explaining those steps and why some decisions were taken along the way.

Although, the original labs this steps were extracted from will be mentioned when appropriate. The order is basically the same as in the original exposition, except that  pre-requisites" and "client tools"sections have been merged together:

* [Prerequisites](/docs/01-prerequisites.md)
* [Compute Resources](/docs/02-compute-resources.md)
* [Provisioning a CA and Generating TLS Certificates](/docs/03-certificate-authority.md)
* [Kubernetes Configuration Files for Authentication](/docs/04-configuration-files.md)
* [Data Encryption Config and Key](/docs/05-encryption-keys.md)
* [Bootstrapping the etcd Cluster](/docs/06-bootstrapping-etcd.md)
* [Bootstrapping the Kubernetes Control Plane](/docs/07-bootstrapping-controllers.md)
* [Bootstrapping the Kubernetes Worker Nodes](/docs/08-bootstrapping-workers.md)
* [Configuring kubectl for Remote Access](/docs/09-setup-kubectl.md)
* [Provisioning Pod Network Routes](/docs/10-pod-network-routes.md)
* [Deploying the DNS Cluster Add-on](/docs/11-dns-addon.md)
* [Smoke Test](/docs/12-smoke-test.md)
* [Cleaning up](/docs/13-cleanup.md)

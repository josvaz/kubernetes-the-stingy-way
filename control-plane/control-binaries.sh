#!/bin/bash
set -e

INDEX=$1
INTERNAL_IP=$(cat "/vagrant/controller-${INDEX}_ip")

ETCD_CLIENT_PORT=2379
ETCD_SERVERS=https://$(cat /vagrant/controller-0_ip):${ETCD_CLIENT_PORT}
MAX_ETCD=$((${NO_CONTROLLERS:=3}-1))
for i in $(seq 0 $MAX_ETCD); do
  ETCD_SERVERS+=,https://$(cat "/vagrant/controller-${i}_ip"):${ETCD_CLIENT_PORT}
  ETCD_SERVERS+=:${ETCD_CLIENT_PORT}
done

echo "Create Kubernetes configuration directory"
sudo mkdir -p /etc/kubernetes/config

echo "Download Kubernetes Control binaries"
download_url='https://storage.googleapis.com/kubernetes-release/release'
kubernetes_version=$(cat /vagrant/kubernetes-version)
wget -q --show-progress --https-only --timestamping \
  --directory-prefix=/usr/local/bin/ \
  "${download_url}/v${kubernetes_version}/bin/linux/amd64/kube-apiserver" \
  "${download_url}/v${kubernetes_version}/bin/linux/amd64/kube-controller-manager" \
  "${download_url}/v${kubernetes_version}/bin/linux/amd64/kube-scheduler" \
  "${download_url}/v${kubernetes_version}/bin/linux/amd64/kubectl"

echo "Install Kubernetes Control binaries"
chmod +x /usr/local/bin/kube-apiserver \
         /usr/local/bin/kube-controller-manager \
         /usr/local/bin/kube-scheduler \
         /usr/local/bin/kubectl

echo "Configure Kubernetes APIServer"
sudo mkdir -p /var/lib/kubernetes/

sudo mv ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
  service-account-key.pem service-account.pem \
  encryption-config.yaml /var/lib/kubernetes/

echo "Configure Kubernetes Controller Manager"
cat <<EOF | sudo tee /etc/systemd/system/kube-apiserver.service
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=${INTERNAL_IP} \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
  --enable-admission-plugins=Initializers,NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
  --enable-swagger-ui=true \\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers="${ETCD_SERVERS}" \\
  --event-ttl=1h \\
  --experimental-encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --kubelet-https=true \\
  --runtime-config=api/all \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

echo "Configure the Kubernetes Controller Manager"
sudo mv kube-controller-manager.kubeconfig /var/lib/kubernetes/

cat <<EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --address=0.0.0.0 \\
  --cluster-cidr=192.168.50.0/24 \\
  --cluster-name=kubernetes \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

echo "Configure the Kubernetes Scheduler"
sudo mv kube-scheduler.kubeconfig /var/lib/kubernetes/

cat <<EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yaml
apiVersion: kubescheduler.config.k8s.io/v1alpha1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF

cat <<EOF | sudo tee /etc/systemd/system/kube-scheduler.service
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

echo "Start the Controller Services"
sudo systemctl daemon-reload
sudo systemctl enable kube-apiserver kube-controller-manager kube-scheduler
sudo systemctl start kube-apiserver kube-controller-manager kube-scheduler

#echo "Waiting 5 seconds to check status..."
#sleep 5
sudo systemctl status --no-pager kube-apiserver kube-controller-manager kube-scheduler

function retry {
  retries=5
  set +e
  set -x
  # shellcheck disable=SC2068
  while ! $@ ; do
    retries=$((retries - 1))
    if (( retries==0 )) ; then
      echo "Too many failures! ^"
      exit 1
    fi
    echo "Retrying soon..."
    sleep 1
  done
  set +x
  set -e
}

if (( INDEX==2 ))
then
  echo "Check kubernetes components status"
  retry kubectl get componentstatuses --kubeconfig admin.kubeconfig
fi

#!/bin/bash
set -e

echo "Install nginx to serve the HTTPS healtz endpoint"
sudo apt-get update && apt-get install -y nginx


echo "Setup the healthz endpoint"
cat > kubernetes.default.svc.cluster.local <<EOF
server {
  listen      80;
  server_name kubernetes.default.svc.cluster.local;

  location /healthz {
     proxy_pass                    https://127.0.0.1:6443/healthz;
     proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
  }
}
EOF

sudo mv kubernetes.default.svc.cluster.local \
  /etc/nginx/sites-available/kubernetes.default.svc.cluster.local

sudo ln -sf /etc/nginx/sites-available/kubernetes.default.svc.cluster.local /etc/nginx/sites-enabled/

echo "Restart nginx to activate the healtz endpoint"
sudo systemctl restart nginx
sudo systemctl enable nginx

echo "Test the local healthz endpoint"
curl -sH "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz

#!/bin/bash
set -e

IP_ADDRESS=$1
ROUTES=$2

echo 'Setup Pod routes'
interface_name=$(ip address | grep "inet ${IP_ADDRESS}" -B2 \
  |grep 'mtu ' | awk '{print $2}' |tr -d ':')

cat <<EOF | sudo tee /etc/netplan/99-routes.yaml
network:
  ethernets:
    ${interface_name}:
      routes:
EOF
for pair in $(echo "${ROUTES}" | tr ',' '\n'); do
  IFS=":" read -r -a route <<< "$pair"
  cat <<EOF | sudo tee -a /etc/netplan/99-routes.yaml
        - to: ${route[0]}
          via: ${route[1]}
EOF
done

echo "Prepared config at /etc/netplan/99-routes.yaml"
cat /etc/netplan/99-routes.yaml
netplan apply
route 
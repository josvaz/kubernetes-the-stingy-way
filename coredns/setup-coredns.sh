#!/bin/bash
set -euo pipefail

source ../shlib/kubewait.sh

echo "Setup coredns in our new kubernetes cluster"
kubectl apply -f coredns.yaml

wait_running 2 kubectl get pods -l k8s-app=kube-dns -n kube-system

echo "Verify coredns setup"
kubectl run --generator=run-pod/v1 busybox --image=busybox:1.28 \
  --command -- sleep 600

wait_running 1 kubectl get pods -l run=busybox

POD_NAME=$(kubectl get pods -l run=busybox \
	   -o jsonpath="{.items[0].metadata.name}")
kubectl exec -ti "${POD_NAME}" -- nslookup kubernetes


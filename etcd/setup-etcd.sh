#!/bin/bash
set -e

INDEX=$1
PORT=2380
CLIENT_PORT=2379

INTERNAL_IP=$(cat "/vagrant/controller-${INDEX}_ip")
ETCD_NAME=$(hostname -s)

CLIENT_URLS="https://${INTERNAL_IP}:${CLIENT_PORT}"
CLIENT_URLS+=",https://127.0.0.1:${CLIENT_PORT}"

CONTROLLER_LIST=controller-0=https://$(cat "/vagrant/controller-0_ip"):${PORT}
for i in 1 2; do
  item=controller-${i}=https://$(cat "/vagrant/controller-${i}_ip"):${PORT}
  CONTROLLER_LIST+=",${item}"
done

echo "Download etcd..."
wget -q --show-progress --https-only --timestamping \
  "https://github.com/coreos/etcd/releases/download/v3.3.10/etcd-v3.3.10-linux-amd64.tar.gz"

echo "Install etcd..."
tar -xvf etcd-v3.3.10-linux-amd64.tar.gz
sudo mv etcd-v3.3.10-linux-amd64/etcd* /usr/local/bin/

echo "Configure etcd..."
sudo mkdir -p /etc/etcd /var/lib/etcd
sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/

echo "Create the etcd service"
cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:${PORT} \\
  --listen-peer-urls https://${INTERNAL_IP}:${PORT} \\
  --listen-client-urls "${CLIENT_URLS}" \\
  --advertise-client-urls https://${INTERNAL_IP}:${CLIENT_PORT} \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster "${CONTROLLER_LIST}" \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

echo "Start etcd"
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd

echo "Check etcd status"
sudo systemctl --no-pager status etcd

if (( INDEX>0 ))
then
  echo "Runnings etcd members so far..."
  sudo ETCDCTL_API=3 etcdctl member list \
    --endpoints=https://127.0.0.1:2379 \
    --cacert=/etc/etcd/ca.pem \
    --cert=/etc/etcd/kubernetes.pem \
    --key=/etc/etcd/kubernetes-key.pem
fi

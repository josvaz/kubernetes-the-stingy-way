#!/bin/bash
set -e
KUBERNETES_PUBLIC_ADDRESS=$(cat ../vagrant/lb-0_ip)

kubectl config set-cluster kubernetes-the-stingy-way \
    --certificate-authority=../certs/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=admin.kubeconfig

kubectl config set-credentials admin \
    --client-certificate=../certs/admin.pem \
    --client-key=../certs/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=admin.kubeconfig

kubectl config set-context default \
    --cluster=kubernetes-the-stingy-way \
    --user=admin \
    --kubeconfig=admin.kubeconfig

kubectl config use-context default --kubeconfig=admin.kubeconfig

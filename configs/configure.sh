#!/bin/bash
set -e
./config-workers.sh
./config-kube-proxy.sh
./config-kube-controller-manager.sh
./config-kube-scheduler.sh
./config-kube-admin.sh

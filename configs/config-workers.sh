#!/bin/bash
set -e
KUBERNETES_PUBLIC_ADDRESS=$(cat ../vagrant/lb-0_ip)

MAX_WORKER=$((${NO_WORKER:=2}-1))
for index in $(seq 0 $MAX_WORKER); do
  instance="worker-${index}"
  kubectl config set-cluster kubernetes-the-stingy-way \
    --certificate-authority=../certs/ca.pem \
    --embed-certs=true \
    --server=https://"${KUBERNETES_PUBLIC_ADDRESS}:6443" \
    --kubeconfig="${instance}.kubeconfig"

  kubectl config set-credentials "system:node:${instance}" \
    --client-certificate="../certs/${instance}.pem" \
    --client-key="../certs/${instance}-key.pem" \
    --embed-certs=true \
    --kubeconfig="${instance}.kubeconfig"

  kubectl config set-context default \
    --cluster=kubernetes-the-stingy-way \
    --user="system:node:${instance}" \
    --kubeconfig="${instance}.kubeconfig"

  kubectl config use-context default --kubeconfig="${instance}.kubeconfig"
done

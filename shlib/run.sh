#!/bin/bash

function run_in_node {
  node=$1
  shift
  cmd=$*
  (cd ../vagrant && vagrant ssh "${node}" --no-tty -c "${cmd}")
}

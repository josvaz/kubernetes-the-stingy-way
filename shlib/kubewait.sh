#!/bin/bash

function wait_running {
  expected=$1
  shift
  cmd=$*
  retries=30
  echo "waiting for $expected running from: $cmd"
  while true; do
    output=$($cmd 2> /dev/null)
    running=$(echo "$output" | grep -c 'Running' || true)
    if ((running==expected)); then
      echo "$output"
      return;
    fi
    retries=$((retries-1))
    if ((retries<1)); then
      echo "Could not get $expected running from: $cmd"
      echo "$output"
      exit 1
    fi
    sleep 1
  done
}

#!/bin/bash
set -e

# need to create placeholder files as vagrant checks for their
# existence even when it is not doing provisioning of them yet
# Those files need to be created by scripts that need to know
# the IPs of the VMs, so the VMs needs to be deployed beforehand.
PLACEHOLDER_FILES="certs/ca.pem"
PLACEHOLDER_FILES+=" certs/ca-key.pem"
PLACEHOLDER_FILES+=" certs/kubernetes.pem"
PLACEHOLDER_FILES+=" certs/kubernetes-key.pem"
PLACEHOLDER_FILES+=" certs/service-account.pem"
PLACEHOLDER_FILES+=" certs/service-account-key.pem"
PLACEHOLDER_FILES+=" certs/worker-0.pem"
PLACEHOLDER_FILES+=" certs/worker-0-key.pem"
PLACEHOLDER_FILES+=" certs/worker-1.pem"
PLACEHOLDER_FILES+=" certs/worker-1-key.pem"
PLACEHOLDER_FILES+=" configs/admin.kubeconfig"
PLACEHOLDER_FILES+=" configs/kube-controller-manager.kubeconfig"
PLACEHOLDER_FILES+=" configs/kube-scheduler.kubeconfig"
PLACEHOLDER_FILES+=" configs/worker-0.kubeconfig"
PLACEHOLDER_FILES+=" configs/worker-1.kubeconfig"
PLACEHOLDER_FILES+=" configs/kube-proxy.kubeconfig"
PLACEHOLDER_FILES+=" cypher/encryption-config.yaml"

for f in ${PLACEHOLDER_FILES}; do
  touch "${f}"
done


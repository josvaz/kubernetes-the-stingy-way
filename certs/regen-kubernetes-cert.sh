#!/bin/bash
KUBERNETES_PUBLIC_ADDRESS=$(cat "../vagrant/vip")

# Needed to start etcd before the LB is setup
SANS=${KUBERNETES_PUBLIC_ADDRESS},127.0.0.1,10.32.0.1,kubernetes.default
SANS+=",$(cat "../vagrant/lb-0_ip")"
for i in 0 1 2; do
    SANS+=",$(cat "../vagrant/controller-${i}_ip")"
done

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname="${SANS}" \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

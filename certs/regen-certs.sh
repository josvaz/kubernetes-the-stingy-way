#!/bin/bash
set -e
./regen-ca.sh
./regen-admin-cert.sh
./regen-client-certs.sh
./regen-controller-manager-cert.sh
./regen-kube-proxy-cert.sh
./regen-kube-scheduler-cert.sh
./regen-kubernetes-cert.sh
./regen-service-account.sh
echo "All cert generated succesfully!"
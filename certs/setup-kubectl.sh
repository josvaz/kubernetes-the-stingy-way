#!/bin/bash
set -e

echo 'Setup kubectl'
KUBERNETES_PUBLIC_ADDRESS=$(cat ../vagrant/vip)

kubectl config set-cluster kubernetes-the-stingy-way \
  --certificate-authority=ca.pem \
  --embed-certs=true \
  --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443

kubectl config set-credentials admin \
  --client-certificate=admin.pem \
  --client-key=admin-key.pem

kubectl config set-context kubernetes-the-stingy-way \
  --cluster=kubernetes-the-stingy-way \
  --user=admin

kubectl config use-context kubernetes-the-stingy-way

kubectl get componentstatuses

kubectl get nodes

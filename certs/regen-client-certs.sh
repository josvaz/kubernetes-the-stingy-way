#!/bin/bash
for instance in worker-0 worker-1; do
cat > ${instance}-csr.json <<EOF
{
  "CN": "system:node:${instance}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "ES",
      "L": "Torrejon de Ardoz",
      "O": "system:nodes",
      "OU": "Kubernetes The Stingy Way",
      "ST": "Madrid"
    }
  ]
}
EOF

EXTERNAL_IP=$(cat "../vagrant/${instance}_ip")

INTERNAL_IP=$(cat "../vagrant/${instance}_ip")

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${instance},${EXTERNAL_IP},${INTERNAL_IP} \
  -profile=kubernetes \
  ${instance}-csr.json | cfssljson -bare ${instance}
done


# Contributing

This **project welcomes contributions**.

If you found a bug fix, you are welcome to report and send us a PR for review.

For PRs, we may suggest some changes or improvements or alternatives.

Some things that will increase the chance that your pull request is accepted:

* Write tests.
* Write a [good commit message][commit].

[commit]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

